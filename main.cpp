#include <iostream>
#include <cstdlib>
#include <thrust/complex.h>
#include <cmath>
#include <type_traits>
#include <png.h>
#include <string>
#include <setjmp.h>
#include "main.h"
#include "mandelomp.h"
#include "mandelcuda.h"

using thrust::complex;

void write_image(std::string filename, int width, int height, png_byte **img)
{
	FILE *f = fopen(filename.c_str(), "wb");
	if (!f) {
		std::cout << "failed to open file " << filename << std::endl;
		return;
	}

	png_struct *png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) {
		std::cout << "failed to create png_ptr" << std::endl;
		fclose(f);
		exit(EXIT_FAILURE);
	}
	png_info *info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		std::cout << "failed to create info_ptr struct" << std::endl;
		goto end;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		std::cout << "libpng internal failure" << std::endl;
		goto end;
	}
	png_init_io(png_ptr, f);
	png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB,
		PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	png_write_info(png_ptr, info_ptr);
	png_write_image(png_ptr, img);
	png_write_end(png_ptr, NULL);
end:
	png_destroy_write_struct(&png_ptr, &info_ptr);
	fclose(f);
}

void usage(void)
{
	std::cout << "uso: mbrot <C0_REAL> <C0_IMAG> <C1_REAL> <C1_IMAG> <W> <H> <CPU/GPU> <THREADS> <SAIDA>"
		<< std::endl;
}

int main(int argc, char **argv)
{
	if (argc != 10) {
		usage();
		return -1;
	}
	float c0_real = std::stof(std::string{argv[1]});
	float c0_imag = std::stof(std::string{argv[2]});
	float c1_real = std::stof(std::string{argv[3]});
	float c1_imag = std::stof(std::string{argv[4]});

	int width = std::stoi(std::string{argv[5]});
	int height = std::stoi(std::string{argv[6]});

	std::string cpu_or_gpu{argv[7]};

	int num_threads = std::stoi(std::string{argv[8]});

	std::string filename{argv[9]};

	complex<float> c0{c0_real, c0_imag}, c1{c1_real, c1_imag};

	png_byte **img = NULL;

	bool is_gpu = false;
	if (cpu_or_gpu == std::string("cpu")) {
		is_gpu = false;
	} else if (cpu_or_gpu == std::string("gpu")) {
		is_gpu = true;
	} else {
		usage();
		return -1;
	}

	if (is_gpu) {
		img = create_pixel_matrix_cuda(c0, c1, width, height, num_threads, filename);
	} else {
		img = create_pixel_matrix_omp(c0, c1, width, height, num_threads, filename);
	}

	write_image(filename, width, height, img);
	if (is_gpu) {
		destroy_img_cuda(img, height);
	} else {
		for (int i = 0; i <= height; ++i) {
			free(img[i]);
		}
		free(img);
	}
}
