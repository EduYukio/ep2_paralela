#ifndef MANDEL_CUDA_H
#define MANDEL_CUDA_H

#include "main.h"
#include <thrust/complex.h>
#include <png.h>

png_byte** create_pixel_matrix_cuda(const thrust::complex<float>& c0, const thrust::complex<float>& c1,
	int width, int height, int num_threads, std::string filename);
void destroy_img_cuda(png_byte** img, int height);

#endif
