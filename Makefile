CXX ?= c++
CUDAC ?= nvcc
CXXFLAGS += $(shell pkg-config --cflags libpng) -I/usr/local/cuda/include/ -fopenmp -g
CUDA_FLAGS += -g
LDLIBS += $(shell pkg-config --libs libpng) -lcuda -lcudart -lcudadevrt -fopenmp
LDFLAGS += -L/usr/local/cuda/lib64
CXX_OBJ := mandelomp.o main.o
CUDA_OBJ := mandelcuda.o
BIN := mbrot
LINK_OBJ := link.o

$(BIN): $(CXX_OBJ) $(CUDA_OBJ) $(LINK_OBJ)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(CUDA_OBJ): %.o: %.cu
	$(CUDAC) --device-c $(CUDA_FLAGS) -o $@ $<

$(LINK_OBJ) : $(CUDA_OBJ)
	$(CUDAC) --device-link $(CUDA_FLAGS) -o $@ $^

$(CXX_OBJ): %.o : %.cpp
	$(CXX) -c $(CXXFLAGS) -o $@ $<

clean:
	rm -f $(BIN) $(CXX_OBJ) $(CUDA_OBJ) $(LINK_OBJ)
