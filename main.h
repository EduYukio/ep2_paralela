#ifndef MANDEL_MAIN_H
#define MANDEL_MAIN_H

#include <complex.h>
#include <png.h>

const int M = 128;

//#define MANDELBUG

#ifdef __CUDACC__
#	define MANDEL_DEVICE __device__
#	define MANDEL_GLOBAL __global__
#else
#	define MANDEL_DEVICE
#	define MANDEL_GLOBAL
#endif

MANDEL_DEVICE
bool is_mandelbrot(thrust::complex<float> c, int& count);

MANDEL_DEVICE
void write_pixel(const thrust::complex<float>& c, png_byte** img, int i, int j);

#endif
