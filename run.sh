#!/usr/bin/env bash
TIMEFORMAT="%R" # imprima o tempo real, em segundos
DIMENSIONS=(100 500 1000 1500)
NUM_THREADS=(100 500 512 4000 4096)
C0_REAL=(0.0 0.5 0.2 0.2 0.3)
C0_IMAG=(0.0 0.0 0.1 0.1 0.4)
C1_REAL=(0.2 0.5 0.0 0.3 0.2)
C1_IMAG=(0.1 0.0 0.0 0.4 0.1)
EXECUTION_COUNT=20

mkdir -p data

function run_per_thread_count() {
	local threads=$1
	local cpu_or_gpu=$2
	for w in "${DIMENSIONS[@]}" ; do
		for h in "${DIMENSIONS[@]}" ; do
			for ((i = 0; i < ${#C0_REAL[@]} ; i++)) ; do
				for ((k = 0; k < "$EXECUTION_COUNT" ; k++)) ; do
					{ time ./mbrot \
						${C0_REAL[i]} ${C0_IMAG[i]} \
						${C1_REAL[i]} ${C1_IMAG[i]} \
						"$w" "$h" "$cpu_or_gpu" \
						"$threads" "foo.png" ; } \
						2>> data/"time_${C0_REAL[i]}"_"${C0_IMAG[i]}"_"${C1_REAL[i]}"_"${C1_IMAG[i]}"_"$w"_"$h"_"$cpu_or_gpu"_"$threads".txt
				done
			done
		done
	done
}

for threads in "${NUM_THREADS[@]}" ; do
	run_per_thread_count "$threads" "cpu"
	run_per_thread_count "$threads" "gpu"
done
