#include <iostream>
#include <string>
#include <thrust/complex.h>
#include <omp.h>
#include <png.h>
#include "main.h"

using thrust::complex;

inline float norma(const complex<float>& c)
{
	return sqrt(c.real() * c.real() + c.imag() * c.imag());
}

bool is_mandelbrot(complex<float> c, int& count)
{
	if (count >= M) {
		return true;
	}
#ifdef MANDELBUG
	std::cout << "norma(c) = " << norma(c) << std::endl;
#endif
	if (norma(c) <= 2) {
		{
			complex<float> old_c{c};
			c *= c;
			c += old_c;
		}
		return is_mandelbrot(c, ++count);
	}
	return false;
}

inline void write_pixel(const complex<float>& c, png_byte** img, int i, int j)
{
	png_byte r, g, b;
	int count = 1;
	if (is_mandelbrot(c, count)) {
		r = g = b = 0x00;
	} else {
		/* a cor se aproxima de preto à medida que count se aproxima de M. */
		r = g = b = static_cast<png_byte>(0xff - (static_cast<float>(count) / M) * 0xc0);
	}
	img[i][3 * j] = r;
	img[i][3 * j + 1] = g;
	img[i][3 * j + 2] = b;
}

png_byte** create_pixel_matrix_omp(const complex<float>& c0, const complex<float>& c1, int width, int height, int num_threads, std::string filename)
{
	omp_set_num_threads(num_threads);

	const float delta_x = (c1.real() - c0.real()) / width;
	const float delta_y = (c1.imag() - c0.imag()) / height;

	png_byte **img;
	img = static_cast<png_byte**>(malloc((height + 1) * sizeof(png_byte *)));
	if(!img){
		std::cout << "Img malloc failed" << std::endl;
		exit(1);
	}
	for (int k = 0; k <= height; k++) {
		img[k] = static_cast<png_byte*>(malloc(3 * (width + 1) * sizeof(png_byte))); // 3 para RGB
		if(!img[k]){
			std::cout << "Img malloc failed" << std::endl;
			exit(1);
		}
	}

#pragma omp parallel for
	for (int i = 0; i <= height; ++i) {
#pragma omp parallel for
		for (int j = 0; j <= width; ++j) {
			complex<float> c{c0.real() + j * delta_x, c0.imag() + i * delta_y};
#ifdef MANDELBUG
			std::cout << "i = " << i << " j = " << j
				<< " c.real = " << c.real()
				<< " c.imag = " << c.imag() << std::endl;
#endif
			write_pixel(c, img, i, j);
		}
	}
	return img;
}
