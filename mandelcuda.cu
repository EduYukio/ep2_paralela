#include <iostream>
#include <string>
#include <thrust/complex.h>
#include <png.h>
#include "main.h"

using thrust::complex;

// https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
#define gpu_error_check(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
	if (code != cudaSuccess) {
		fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) {
			exit(code);
		}
	}
}

__device__
float norma_cuda(const complex<float>& c)
{
	return sqrt(c.real() * c.real() + c.imag() * c.imag());
}

__device__
bool is_mandelbrot_cuda(complex<float> c, int& count)
{
	if (count >= M) {
		return true;
	}
	if (norma_cuda(c) <= 2) {
		{
			complex<float> old_c{c};
			c *= c;
			c += old_c;
		}
		return is_mandelbrot_cuda(c, ++count);
	}
	return false;
}

__device__
void write_pixel_cuda(const complex<float>& c, png_byte** img, int i, int j)
{
	png_byte r, g, b;
	int count = 1;
	if (is_mandelbrot_cuda(c, count)) {
		r = g = b = 0x00;
	} else {
		/* a cor se aproxima de preto à medida que count se aproxima de M. */
		r = g = b = static_cast<png_byte>(0xff - (static_cast<float>(count) / M) * 0xc0);
	}
	img[i][3 * j] = r;
	img[i][3 * j + 1] = g;
	img[i][3 * j + 2] = b;
}

__global__
// pass c0 and c1 by value (don't copy references from the CPU)
void write_all_pixels(complex<float> c0, complex<float> c1, int width, int height, png_byte **img)
{
	const float delta_x = (c1.real() - c0.real()) / width;
	const float delta_y = (c1.imag() - c0.imag()) / height;

	for (int i = blockIdx.x; i <= height; i += gridDim.x) {
		for (int j = threadIdx.x; j <= width; j += blockDim.x) {
			complex<float> c{c0.real() + j * delta_x, c0.imag() + i * delta_y};
			write_pixel_cuda(c, img, i, j);
		}
	}
}

png_byte** create_pixel_matrix_cuda(const complex<float>& c0, const complex<float>& c1, int width, int height, int num_threads, std::string filename)
{
	png_byte **img = NULL;

	cudaMallocManaged(&img, (height + 1) * sizeof(png_byte *));
	for (int k = 0; k <= height; k++) {
		gpu_error_check(cudaMallocManaged(&img[k], 3 * (width + 1) * sizeof(png_byte))); // 3 para RGB
	}

	write_all_pixels<<<height + 1, num_threads>>>(c0, c1, width, height, img);
	gpu_error_check(cudaDeviceSynchronize());
	return img;
}

void destroy_img_cuda(png_byte** img, int height)
{
	for (int k = 0; k <= height; k++) {
		gpu_error_check(cudaFree(img[k]));
	}
	gpu_error_check(cudaFree(img));
}
