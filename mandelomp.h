#ifndef MANDEL_OMP_H
#define MANDEL_OMP_H

#include <string>
#include <thrust/complex.h>
#include <png.h>
#include "main.h"

png_byte** create_pixel_matrix_omp(const thrust::complex<float>& c0, const thrust::complex<float>& c1,
	int width, int height, int num_threads, std::string filename);

#endif
